#sum column wise

BEGIN {FS="|";OFS="|"}
{
	sum=0;
	for(i=3;i<=NF;i++)
	{
		sum=sum+$i;      
	}
	print $0, sum;     
}
