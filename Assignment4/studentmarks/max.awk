#calculate max min  mean standard deviation
BEGIN {FS="|";t=2;OFS="|"}
{
	t++;
	if(t==3) 
	for(i=3;i<=NF;i++)
	{
		min[i] = $i;
		max[i] = $i;
	}
	for(i=3;i<=NF;i++)
	{
		f[i] +=$i;
		g[i] +=($i*$i);
		if(min[i]>$i)
		min[i]=$i;
		if(max[i]<$i)
		max[i]=$i;
	}
	print$0;
}
END{
printf("max    |   ");
printf("    |   %d",max[3]);
for(j=4;j<=NF;j++)
printf(" |   %d",max[j]);
printf("\nmin    |   ");
printf("    |   %d",min[3]);
for(j=4;j<=NF;j++)
printf(" |   %d",min[j]);
printf("\nmean   |  ");
printf("     | %.2f",f[3]/NR);
for(j=4;j<=NF;j++)
printf(" | %.2f",f[j]/NR);
printf("\nsd     |  ");
printf("     | %.2f",sqrt((g[3]/NR)-((f[3]/NR)*(f[3]/NR))));
for(j=4;j<=NF;j++)
printf(" | %.2f",sqrt((g[j]/NR)-((f[j]/NR)*(f[j]/NR))));
print "\n";


}
