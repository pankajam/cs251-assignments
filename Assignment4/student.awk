BEGIN {FS="|";r=2;OFS="|"}
{
	r++;
	if(r==3) 
	for(p=3;p<=NF;p++)
	{	min[p] = $p;
		max[p] = $p;
	}
	for(p=3;p<=NF;p++)
	{
		a[p] +=$p;
		b[p] +=($p*$p);
		if(min[p]>$p)
		min[p]=$ip;
		if(max[p]<$p)
		max[p]=$p;
	}

	add=0;
	for(p=3;p<=NF;p++)
	{
		add=add+$p;      

	}
	print $0, add;     
	
}
END{
printf("max    |   ");
printf("    |   %d",max[4]);
for(q=4;q<=NF;q++)
printf(" |   %d",max[q]);
printf("\nmin    |   ");
printf("    |   %d",min[4]);
for(q=4;q<=NF;q++)
printf(" |   %d",min[q]);
printf("\nmean   |  ");
printf("     | %.2f",a[4]/NR);
for(q=4;q<=NF;q++)
printf(" | %.2f",a[q]/NR);
printf("\nsd     |  ");
printf("     | %.2f",sqrt((b[4]/NR)-((a[4]/NR)*(a[4]/NR))));
for(q=4;q<=NF;q++)
printf(" | %.2f",sqrt((b[q]/NR)-((a[q]/NR)*(a[q]/NR))));   
	
print "\n";


}
