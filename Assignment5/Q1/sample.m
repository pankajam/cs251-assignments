x = [0:1:50];
binomial = binopdf(x, 50, 0.3);
poission = poisspdf(x, 15);
gaussian = normpdf(x, 15, sqrt(10.5));
x = x';
binomial = binomial';
poission = poission';
gaussian = gaussian';
a = [x, binomial, poission, gaussian];
save bin_sample.m a;

