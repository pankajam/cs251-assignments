set term postscript eps enhanced color 24
set output 'graph.eps'

set xlabel "x"		
set ylabel "Function"

set title "Different Distributions"

set key top right

set label "{/Symbol l} = 15" at 15,0.10
set label "{/Symbol m} = 15" at 17,0.12
set label "{np} = 15" at 15,0.13



plot 'bin_sample.m' u 1:2 title "Binomial" w lp, 'bin_sample.m' u 1:3 title "Poisson" w lp, 'bin_sample.m' u 1:4 title "Gaussian" w lp
