#!/usr/bin/gnuplot

set term postscript eps enhanced 24

set output 'gaus1.eps'

set title "Gaussion Histogram"

n = 50;
max = 50;
min = 0;
width = 1.1;

hist(x) = floor(x)+0.5;
set xrange [0:50]
set style fill solid 0.5

set xlabel "x"
set ylabel "Frequency"

plot 'gaus1_sample.m' u (hist($1)):(1) title "Gaussion" smooth freq w boxes lc rgb"green"
